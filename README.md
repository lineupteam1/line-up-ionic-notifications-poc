how to : 

* variables : (dont forget to hava java 8 selected)
```shell script
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export ANDROID_HOME=/home/mathieu/Android/Sdk
export ANDROID_SDK_ROOT=/home/mathieu/Android/Sdk
export GRADLE_HOME=/opt/gradle/gradle-5.0/bin
export PATH="$PATH:$ANDROID_HOME:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$GRADLE_HOME"
```

* to add the folder platforms/
```shell script
$ ionic cordova platform add android
```

* to run the app on the mobile device with live reload
```shell script
$ ionic cordova run android -l
```

* in case of problems
```shell script
$ ionic repair
```

* to install the app on the device
```shell script
$ ionic cordova build android --debug --device 
$ adb install platforms/android/app/build/outputs/apk/debug/app-debug.apk
```

* debug using chrome: 

go to : chrome://inspect/#devices
