import { Component } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private localNotifications: LocalNotifications) {}


  public onClickRegister() {
    console.log('On click register');

    this.localNotifications.schedule({
     id: 1,
     text: 'Single Local Notification',
     foreground: true,
     data: { secret: 'secret' }
   });
  }
}
